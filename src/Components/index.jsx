import React, { Component } from "react";

import "./style.css";

const arrProduct = [
  {
    id: 1,
    price: 30,
    name: "GUCCI G8850U",
    url: "./glassesImage/v1.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 2,
    price: 50,
    name: "GUCCI G8759H",
    url: "./glassesImage/v2.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 3,
    price: 30,
    name: "DIOR D6700HQ",
    url: "./glassesImage/v3.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 4,
    price: 30,
    name: "DIOR D6005U",
    url: "./glassesImage/v4.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 5,
    price: 30,
    name: "PRADA P8750",
    url: "./glassesImage/v5.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 6,
    price: 30,
    name: "PRADA P9700",
    url: "./glassesImage/v6.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 7,
    price: 30,
    name: "FENDI F8750",
    url: "./glassesImage/v7.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 8,
    price: 30,
    name: "FENDI F8500",
    url: "./glassesImage/v8.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 9,
    price: 30,
    name: "FENDI F4300",
    url: "./glassesImage/v9.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },
];

class ThuKinh extends Component {
  //properties
  state = {
    glassesImage: arrProduct[0].url,
    glassesDescription: arrProduct[0].desc,
    glassesName: arrProduct[0].name,
  };

  //methods

  changeGlasses = (index) => {
    this.setState(
      {
        glassesImage: arrProduct[index].url,
        glassesDescription: arrProduct[index].desc,
        glassesName: arrProduct[index].name,
      },
      console.log(this.state.glassesName)
    );
  };

  renderGlassesList = () => {
    return arrProduct.map((item, index) => {
      return (
        <div className="glasses" key={index}>
          <img
            src={item.url}
            width="100px"
            onClick={() => {
              this.changeGlasses(index);
            }}
          />
        </div>
      );
    });
  };

  render() {
    return (
      <div className="wrapper">
        <div className="layer">
          <header>
            <h1>TRY ON GLASSES ONLINE APP</h1>
          </header>
          <div className="body">
            <div className="model-wrapper">
              <div className="model-photo">
                <img className="model-wearing" src="./model.jpg" alt="" />
                <img
                  className="glasses-worn"
                  src={this.state.glassesImage}
                  alt=""
                />
                <div className="glasses-info">
                  <h4 className="glasses-name">{this.state.glassesName}</h4>
                  <p className="glasses-desc">
                    {this.state.glassesDescription}
                  </p>
                </div>
              </div>

              <div className="model-photo">
                <img src="./model.jpg" alt="" />
              </div>
            </div>

            <div className="glasses-wrapper">{this.renderGlassesList()}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default ThuKinh;
